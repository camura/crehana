from django.contrib import messages


class Cart(object):

    MESSAGE_ADD = u'Se agrego el curso %s con exito'
    TAG_ADD = 'alert alert-info'

    MESSAGE_REMOVE = u'Se elimino el curso con exito'
    TAG_ERROR = 'alert alert-danger'

    @staticmethod
    def add(request, course):
        cart = request.session.get('cart', dict())
        course_id = course.id
        cart[course_id] = dict()
        cart[course_id]['id'] = course_id
        cart[course_id]['name'] = course.name
        if course_id in cart:
            request.session['cart'] = cart
        messages.add_message(request, messages.INFO, Cart.MESSAGE_ADD % course.name, extra_tags=Cart.TAG_ADD)
        return True

    @staticmethod
    def remove(request, course_id):
        try:
            request.session.modified = True
            del request.session['cart'][course_id]
            messages.add_message(request, messages.ERROR, Cart.MESSAGE_REMOVE, extra_tags=Cart.TAG_ERROR)
        except Exception:
            return True

    @staticmethod
    def all(request):
        return request.session.get('cart', dict())

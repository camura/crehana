# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from models.course import Course

# Register your models here.


class CourseAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


admin.site.register(Course, CourseAdmin)

from django.views.generic import ListView, RedirectView, TemplateView
from apps.learning.models.course import Course
from django.urls import reverse
from logic.cart import Cart
from apps.learning.models import Course


class CoursesListView(ListView):

    template_name = 'courses/list.html'
    model = Course
    paginate_by = 8


class CoursesAddView(RedirectView):

    def get_redirect_url(self, *args, **kwargs):
        return reverse('learning_courses_list')

    def get(self, request, *args, **kwargs):
        response = super(CoursesAddView, self).get(request, *args, **kwargs)
        course_id = self.kwargs.get('course_id')
        course = Course.objects.get(id=course_id)
        Cart.add(request, course)
        return response


class CoursesRemoveView(RedirectView):

    def get_redirect_url(self, *args, **kwargs):
        return reverse('learning_courses_show')

    def get(self, request, *args, **kwargs):
        response = super(CoursesRemoveView, self).get(request, *args, **kwargs)
        course_id = self.kwargs.get('course_id')
        Cart.remove(request, course_id)
        return response


class CoursesShowView(TemplateView):

    template_name = 'courses/show.html'

    def get_context_data(self, **kwargs):
        context = super(CoursesShowView, self).get_context_data(**kwargs)
        courses = Cart.all(self.request)
        context['courses'] = courses
        return context

from django.conf.urls import include, url
from views.courses import CoursesListView, CoursesAddView, CoursesRemoveView, CoursesShowView


urlpatterns = [
    url(r'^$',
        CoursesListView.as_view(),
        name='learning_courses_list'),
    url(r'^add/(?P<course_id>[0-9]+)/$',
        CoursesAddView.as_view(),
        name='learning_courses_add'),
    url(r'^remove/(?P<course_id>[0-9]+)/$',
        CoursesRemoveView.as_view(),
        name='learning_courses_remove'),
    url(r'^show/$',
        CoursesShowView.as_view(),
        name='learning_courses_show'),
]


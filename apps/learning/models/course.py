# -*- coding: utf-8 -*
from django.db import models


class Course(models.Model):

    name = models.CharField (
        max_length=150,
        verbose_name='Nombre',
        null=False,
        default=None
    )

    class Meta:
        verbose_name = 'Curso'
        verbose_name_plural = 'Cursos'